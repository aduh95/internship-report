# Internship presentation

![Groupe ESEO](./img/eseo.svg)

Web Programing Engineer & Data Analyst

Web & Software Development

![Schneider Electric](./img/schneider-electric-logo.svg)

---

<!-- Annonce du plan: 1mn -->

## Context

<!-- Annonce du plan: 1mn -->

### SE

- Ce n'est pas une marque allemande d'ascenseurs
- Les départements: EcoStruxure, CyberSecurity, Microgrids, Solar&Energy
  Storage, etc.
- EcoStruxure
  https://www.schneider-electric.com/en/work/campaign/innovation/overview.jsp
- EcoBuilding: EcoXpert ?

### EcoBuilding team

World map avec le gang

- Hong-Kong
- Montpellier
- Rueil-Malmaison
- Grenoble
- Boston
- SF
- Austin, TX
- New Delhi
- Brussels

> Non technical team

### Tools used internally

????

## Accomplishments

### Liferay (SmartSpace)

Prise en main d'un outil legacy Automatisation de la création de page à partir
de fichiers utilisateurs

Parler des analytics

### Lithium (cross browser challenges)

Intégration web: IE, vieux Chrome, Récents browsers

### Showpad

#### 01Click bad practices

> LOL les images vertes LOL les aveugles

#### React app and future challenges

> Test

> Future proof? (i18n, Yarn PR, ...)

### IDMS update

> Project manage inside EcoBuilding the IDMS transition

## Summary and outcome

- The experience of working for big corporation
- The application of the knowledge acquired at ESEO in the professional world
- Develop my project management skills

---

Thank you
