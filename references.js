"use strict";

function replaceFromText(text, element) {
  for (const em of document.querySelectorAll("em")) {
    if (em.textContent === text) {
      em.parentNode.replaceChild(element, em);
      break;
    }
  }
}

/**
 * Acronyms
 */
fetch(
  "/media/%2Fhome%2Fduhamean%2FDocuments%2Fschneider%2Finternship-report%2Facronyms.json"
)
  .then(response => (response.ok ? response.json() : Promise.reject()))
  .then(acronyms => {
    for (const acronym of Object.keys(acronyms)) {
      const xPathResults = document.evaluate(
        `//text()[contains(.,'${acronym}')]`,
        document
      );
      const acronymRegex = new RegExp(`(?:^|\\W)${acronym}(?:$|\\W)`);
      const acronymLink = document.createElement("a");
      const textNodes = (function() {
        var textNode;
        const textNodes = [];
        while ((textNode = xPathResults.iterateNext())) {
          const { wholeText } = textNode;
          if (acronymRegex.test(wholeText)) {
            textNodes.push(textNode);
          }
        }
        return textNodes;
      })();

      acronymLink.className = "dynamic";
      acronymLink.style.color = "inherit";
      acronymLink.style.fontWeight = "bold";
      acronymLink.href = "#acronyms-used-in-this-document";

      for (const textNode of textNodes) {
        const { parentNode } = textNode;
        let result;
        while ((result = acronymRegex.exec(textNode.wholeText))) {
          const replacmentNode = textNode.splitText(
            result.index ? result.index + 1 : 0
          );

          parentNode
            .insertBefore(acronymLink.cloneNode(), replacmentNode)
            .appendChild(document.createTextNode(acronym));
          parentNode.replaceChild(
            document.createTextNode(
              replacmentNode.wholeText.slice(acronym.length)
            ),
            replacmentNode
          );
        }
      }
    }

    const referenceList = document.createElement("ol");
    referenceList.style.margin = "0";
    referenceList.style.padding = "0";
    for (const [acronym, { description, definition }] of Object.entries(
      acronyms
    )) {
      const referenceListItem = document.createElement("li");

      referenceListItem.appendChild(document.createTextNode(acronym));
      referenceListItem.appendChild(document.createTextNode(": "));
      referenceListItem.appendChild(document.createTextNode(description));

      if (definition) {
        const link = document.createElement("a");
        link.className = "dynamic";
        link.href = definition;
        link.appendChild(document.createTextNode(definition));

        referenceListItem.appendChild(document.createTextNode(" ("));
        referenceListItem.appendChild(link);
        referenceListItem.appendChild(document.createTextNode(")"));
      }

      referenceList.appendChild(referenceListItem);
    }

    replaceFromText("Insert Acronyms", referenceList);
  })
  /**
   * References
   */
  .then(() => {
    let referenceID = 0;
    const referenceList = document.createElement("ol");
    referenceList.style.listStyle = "none";
    referenceList.style.margin = "0";
    referenceList.style.padding = "0";
    const links = document.querySelectorAll("p a:not(.dynamic)");
    for (const link of links) {
      const referenceListItem = document.createElement("li");
      const citation = document.createElement("span");
      const reference = document.createElement("a");
      reference.className = "dynamic";
      reference.style.verticalAlign = "super";
      reference.style.fontSize = "smaller";
      reference.appendChild(document.createTextNode(`[${++referenceID}]`));
      const referenceRecall = reference.cloneNode(true);

      for (const child of link.childNodes) {
        requestAnimationFrame(() => citation.appendChild(child));
      }

      reference.href = `#reference-${referenceID}-1`;
      reference.id = `reference-${referenceID}-0`;
      referenceRecall.id = `reference-${referenceID}-1`;
      referenceRecall.href = `#reference-${referenceID}-0`;

      link.parentNode.insertBefore(reference, link);
      link.parentNode.insertBefore(citation, reference);

      referenceListItem.appendChild(referenceRecall);
      referenceListItem.appendChild(document.createTextNode(": "));
      referenceListItem.appendChild(link);

      link.appendChild(document.createTextNode(link.href));

      referenceList.appendChild(referenceListItem);
    }

    replaceFromText("Insert References", referenceList);
  })
  .then(() => {
    const referenceList = document.createElement("ol");
    referenceList.style.margin = "0";
    referenceList.style.padding = "0";

    const links = document.querySelectorAll("figure>figcaption");
    for (const caption of links) {
      const referenceListItem = document.createElement("li");
      referenceListItem.appendChild(
        document.createTextNode(caption.textContent)
      );

      referenceList.appendChild(referenceListItem);
    }

    replaceFromText("Insert Legends", referenceList);
  })
  .then(console.log, console.error);

setTimeout(() => {
  const picture = document.getElementById("detailed-activity-report")
    .nextElementSibling.firstElementChild;

  picture.style.marginBottom = "150px";
  picture.firstElementChild.style.transform = "scale(2.5)";
  picture.firstElementChild.style.transformOrigin = "top";
}, 999);
